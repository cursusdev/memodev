# Site memoDev

Connaissances en vracs sans fil directeur permet de retrouver des connaissances de tous les jours.


> Quelles que soient les connaissances que nous acquérons, quelques étrangères qu'elles paraissent à nos goûts, à nos habitudes, à notre position même, il arrive toujours un moment où elles nous deviennent utiles.</quote>
<cite>Constance de Théis ; Les pensées diverses (1835)</cite>


Se placer dans le dossier racine du site pour lancer le server :
```
$ jekyll serve
```
