---
layout: fiches
title: Base du language Markdown
permalink: /markdown
---

### Base du language Markdown

#### 1.Paragraphes

    Ceci est paragraphe     =>    <p>Ceci est un paragraphe</p>
Nota: Pour ne sauter qu'une ligne au sein d'un paragraphe, terminer votre ligne avec deux espaces avant un retour à la ligne.

#### 2.En gras

    **Phrase en gras** ou __Phrase en gras__  =>  <strong>Phrase en gras</strong>

#### 3.En Italique

    *Phrase en italique* ou _Phrase en italique_  =>  <em>Phrase en italique</em>

#### 4.Les titres :
##### 4.1.Avec 6 niveaux :
    # Titre de niveau 1         => <h1>Titre de niveau 6</h1>
    ## Titre de niveau 2        => <h2>Titre de niveau 2</h2>
    ### Titre de niveau 3       => <h3>Titre de niveau 3</h3>
    #### Titre de niveau 4      => <h4>Titre de niveau 4</h4>
    ##### Titre de niveau 5     => <h5>Titre de niveau 5</h5>
    ###### Titre de niveau 6    => <h6>Titre de niveau 6</h6>

##### 4.2.Avec 2 niveaux :
    Titre
    -
    Titre
    =

#### 5.Les listes
    * Item 1
    * Item 2
    * Item 3

    <ul>
        <li>Item 1</li>
        <li>Item 2</li>
        <li>Item 3</li>
    </ul>

