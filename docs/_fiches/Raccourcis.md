---
layout: fiches
title: Raccourcis et paramètres Visual Studio Code
permalink: /vscode
---

### 1.Visual studio Code :
#### Principaux raccourcis

    CTRL+Z				    # Modifier Annuler (Edit.Redo)
    CTRL+Y				    # Modifier Rétablir (Edit.Undo)
    ALT+MAJ+(fleche haut/bas)	    # Deplace la selection d'un bloc texte
    CTRL+B				    # Dépace le volet explorer droite/gauche
    CTRL+Shift+Enter		    # Insert une ligne vers le bas
    CTRL+Shift+K			    # Supprime la ligne
    CTRL+ <-			    # Sélectionne la partie de gauche
    CTRL+L				    # Sélectionne la ligne courante
    CTRL+F2				    # Sélectionne toutes les occurences
    CTRL+Shift+P                        # Ouvre la palette de commande
    CTRL+P                              # Liste les fichiers récents comme raccourcis

#### Ma configuration :
```json
{
    "files.autoSave": "afterDelay",
    "editor.fontSize": 12, // Défini la taille de police
    "editor.wordWrap": "on", // Active le retour à la ligne du texte
    "terminal.integrated.fontSize": 12, // Défini la taille de police du Terminal intégré
    "editor.renderControlCharacters": true, // Affiche les caractères de contrôle
    "editor.renderWhitespace": "all", // Affiche les espaces dans l'éditeur
    "editor.renderIndentGuides": true, // Affiche les guides d'indentation
    "editor.minimap.enabled": true, // Affiche un "minimap" de l'éditeur
    "window.title": "${dirty}${activeEditorMedium}${separator}${rootName}${separator}${appName}", // Config. titre fenêtre (Chermin du fichier - projet - Visual Studio Code)
    "terminal.external.windowsExec": "C:\\WINDOWS\\System32\\cmd.exe",
    "files.exclude": { // Fichiers exclus lors des recherches / remplacements
    "**/.git": true,
    "**/.svn": true,
    "**/.hg": true,
    "**/.DS_Store": true,
    "**/vendor": false
    },
    "explorer.openEditors.visible": 0,
    "window.zoomLevel": 0,
    "terminal.integrated.shell.windows": "C:\\Program Files\\Git\\bin\\bash.exe",
    "workbench.editor.enablePreview": false,
    "emmet.includeLanguages": {},
    "workbench.iconTheme": "vs-seti",
    "git.path": "C:\\Program Files\\Git",
    "liveServer.settings.donotShowInfoMsg": true,
    "liveServer.settings.donotVerifyTags": true,
    "explorer.confirmDelete": false,
    "explorer.confirmDragAndDrop": false,
    "editor.parameterHints.enabled": false,
    "vscode-mysql.enableTelemetry": false
}
```