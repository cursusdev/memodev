---
layout: fiches
title: Liens utiles par catégories
permalink: /categories
---

## Liens utiles par catégories

#### 1. Sécurité informatique :

[Hadopi (source)](https://hadopi.fr/) -- Haute Autorité pour la diffusion des œuvres et la protection des droits sur internet

[OWASP (source)](https://www.owasp.org/index.php/Main_Page) -- la communauté de sécurité logicielle libre et ouverte

[CNIL (source)](https://www.cnil.fr/) -- Commission nationale de l'informatique et des libertés

[Godfrain (source)](https://fr.wikipedia.org/wiki/Loi_Godfrain) -- la première loi française réprimant les actes de criminalité informatique et de piratage.

#### 2.Environnement de développement :

[Plate-forme (source)](https://fr.wikipedia.org/wiki/Plate-forme_(informatique)) -- En informatique, une plate-forme est un environnement permettant la gestion et/ou l'utilisation de services applicatifs.

[Système d'exploitation (source)](https://fr.wikipedia.org/wiki/Plate-forme_(informatique)) -- un **système d'exploitation** est un ensemble de programmes qui dirige l'utilisation des ressources d'un ordinateur par des logiciels applicatifs.

[Application Web (source)](https://fr.wikipedia.org/wiki/Application_web) -- une **application web** est une application manipulable directement en ligne grâce à un navigateur web et qui ne nécessite donc pas d'installation sur les machines clientes, contrairement aux applications mobiles.

[Système de gestion de contenu (source)](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_contenu) -- Un système de gestion de contenu ou SGC (CMS en anglais) est une famille de logiciels destinés à la conception et à la mise à jour dynamique de sites Web ou d'applications multimédia. 

#### 3.Documentation Front-End :

[Web frontal (source)](https://fr.wikipedia.org/wiki/D%C3%A9veloppement_web_frontal) -- **Le développement web frontal** correspond aux productions HTML, CSS et JavaScript d’une page internet ou d’une application

[W3C (source)](https://www.w3.org/TR/) -- Le World Wide Web Consortium, abrégé par le sigle W3C, est un organisme de standardisation à but non lucratif, fondé en octobre 1994 chargé de promouvoir la compatibilité des technologies du World Wide Web telles que HTML5, HTML, XHTML, XML, RDF, SPARQL, CSS, XSL, PNG, SVG et SOAP.

[Client (source)](https://fr.wikipedia.org/wiki/Client_%28informatique%29) -- Dans un réseau informatique, un **client** est le logiciel qui envoie des demandes à un serveur.

[HTTP (source)](https://fr.wikipedia.org/wiki/Hypertext_Markup_Language) -- L’**HyperText Markup Language**, généralement abrégé **HTML**, est le langage de balisage conçu pour représenter les pages web

[Javascript (source)](https://fr.wikipedia.org/wiki/JavaScript) -- **JavaScript** est un langage de programmation de scripts principalement employé dans les pages web interactives mais aussi pour les serveurs avec l'utilisation de Node.js

[CSS (source)](https://fr.wikipedia.org/wiki/Feuilles_de_style_en_cascade) -- Les **feuilles de style en cascade**1, généralement appelées **CSS** de l'anglais **Cascading Style Sheets**, forment un langage informatique qui décrit la présentation des documents HTML et XML.

#### 4.Documentation Back-End :

[Développeur full stack (source)](https://fr.wikipedia.org/wiki/D%C3%A9veloppeur_full_stack) -- Un **développeur full stack** est un informaticien capable de réaliser des tâches à n'importe quel niveau technique de la pile des différentes couches qui constituent une application informatique.

[Base de données (source)](https://fr.wikipedia.org/wiki/Base_de_donn%C3%A9es) -- Une **base de données**, permet de stocker et de retrouver l'intégralité de données brutes ou d'informations en rapport avec un thème ou une activité.

[Serveur informatique (source)](https://fr.wikipedia.org/wiki/Serveur_informatique) -- Un **serveur informatique** est un dispositif informatique (matériel ou logiciel) qui offre des services, à un ou plusieurs clients (parfois des milliers).

#### 5.Développement Serveur :

[Node.js (source)](https://nodejs.org/en/) -- Node.js est une **plateforme logicielle** libre et événementielle en JavaScript orientée vers les applications réseau qui doivent pouvoir monter en charge.

[Express (source)](https://expressjs.com/fr/) -- Express est une **infrastructure d'applications Web** Node.js minimaliste et flexible qui fournit un ensemble de fonctionnalités robuste pour les applications Web et mobiles.

#### 6.Développement multi-platform :
##### 6.1.Applications mobiles :
[Cordova (source)](https://cordova.apache.org/)

[PhoneGap (source)](https://phonegap.com/)

[Ionic (source)](https://ionicframework.com/)

[React (source)](https://facebook.github.io/react-native/)

##### 6.2.Applications desktop :
[Node-Webkit (source)](https://nwjs.io/) -- NW.js (précédemment connu sous le nom de node-webkit) vous permet d’appeler tous les modules Node.js directement à partir de DOM et offre une nouvelle façon d’écrire des applications avec toutes les technologies Web.

[Electrons (source)](https://electronjs.org/) -- Permet de développez des applications desktop multi-plateformes avec JavaScript, HTML et CSS.

#### 7.Framework Front-end : 

[Angular (source)](https://angular.io/)

[React (source)](https://angular.io/)

[Vue.js (source)](https://vuejs.org/)

[Vanilla JS (source)](http://vanilla-js.com/) -- Vanilla JS est un framework rapide, léger et multi-plateforme pour construire des applications JavaScript incroyables et puissantes.


#### 8.Editeurs de texte :

[Visual Studio Code (source)](https://code.visualstudio.com/) -- **Visual Studio Code** est un éditeur de code extensible développé par Microsoft pour Windows, Linux et macOS.

[Brackets (source)](http://brackets.io/) -- **Brackets** est un éditeur open source pour le web design et le développement sur des technologies Web telles que HTML, CSS et JavaScript.

[Sublime Text (source)](https://www.sublimetext.com/) -- Sublime Text est un éditeur de texte générique codé en C++ et Python, disponible sur Windows, Mac et Linux.

#### 9.Editeurs en ligne :

[CodePen (source)](https://codepen.io/) -- **CodePen** est une communauté en ligne permettant de tester et de présenter des extraits de code HTML, CSS et JavaScript créés par les utilisateurs.

[JSFindle (source)](https://jsfiddle.net/) -- **JSfiddle** permet de tester et de présenter des extraits de code HTML, CSS et JavaScript créés par les utilisateurs.

[StackBlitz (source)](https://stackblitz.com/) -- L'EDI en ligne pour les applications Web. Propulsé par Visual Studio Code. Créez, partagez et intégrez des projets en direct.

[JS Bin (source)](https://jsbin.com/?html,output) -- Un pastebin en direct pour HTML, CSS et JavaScript et une gamme de processeurs, y compris SCSS, CoffeeScript, Jade et plus encore ...


#### 10. Web components :

[Web components (source)](https://www.webcomponents.org/)

#### 11.Auto-formations :

[DeviensDev (source)](https://deviens.dev/)

[OpenClassRooms (source)](https://openclassrooms.com/fr/)

[FreeCodeCamp (source)](https://www.freecodecamp.org/)

[Udemy (source)](https://www.udemy.com/)

[Coursera (source)](https://www.coursera.org/)

#### 12.Ressources

[MDN (source)](https://developer.mozilla.org/fr/) -- Une communauté de développeurs travaillant ensemble à faire de MDN (réseau de développeur Mozilla) une ressource **exceptionnelle** pour les développeurs qui utilisent les technologies libres du Web..

[W3School (source)](https://www.w3schools.com/) -- W3Schools est un site Web destiné à l'apprentissage en ligne des technologies Web. 
