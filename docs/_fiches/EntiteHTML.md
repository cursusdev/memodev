---
layout: fiches
title: Entités HTML
permalink: /entites
---

## Les entités du XML et de l'HTML par code texte et code numérique de caractère :

### 1.Les cinqs lettres réservés du XML :

```&quot; ou &#34;```       représente le guillemet droit           <code>&#34;</code>
```&amp;  ou &#38;```        représente le logogramme &              <code>&#38;</code>
```&apos; ou &#39;```       représente l'apostrophe                 <code>&#39;</code>
```&lt; ou &#60;```         représente le signe moins que           <code>&#60;</code>
```&gt; ou &#62;```         représente le signe plus grand que      <code>&#62;</code>

### 2.Entités caractère de HTML :
#### 2.1.Les trois lettres réservés du HTML :

```&lt; ou &#60;```         représente le signe moins que           <code>&#60;</code>
```&gt; ou &#62;```         représente le signe plus grand que      <code>&#62;</code>
```&amp; ou &#38;```        représente le logogramme &              <code>&#38;</code>

Les numéros unicodes sont les numéros de ces caractères dans les polices «unicode». Comme il s'agit d'une numérotation sur 2 octets, ces numéros peuvent aller jusqu'à 65000 (en décimal). On peut les employer pour former des caractères en les plaçant entre un <code>&</code> plus le code initial et un point virgule final, ou bien, sous forme hexadécimale, entre un <code>&#</code> plus le code hexadécimale et un point-virgule.

##### 2.2.Exemples de Code texte et de code numérique de caractère :

```# ou &#35;```           représente le symbole du dièse          <code>&#35;</code>
```&le; ou &#8804;```       représente le signe inférieur ou éqal   <code>&#8804;</code>
```&ge; ou &#8805;```       représente le signe supérieur ou éqal   <code>&#8805;</code>
```&sum; ou &#8721;```      représente le signe différence du Sigma <code>&#8721;</code>
```&euro; ou &#8364;```     représente le symbole de l'euro         <code>&#8364;</code>
```&copy; ou &#169;```      représente le symbole copyright         <code>&#169;</code>
```&ne; ou &#8800;```       représente le symbole de différent de   <code>&#8800;</code>
```&equiv; ou &#8801;```    représente le symbole identique à       <code>&#8801;</code>
```&micro; ou &#181;```     représente le symbole de micron         <code>&#181;</code>
```&empty; ou &#8709;```    représente l'ensemble vide ou nul       <code>&#8709;</code>
```&radic; ou &#8730;```    représente la racine carrée             <code>&#8730;</code>
```&sup2; ou &#178;```      représente le carré                     <code>&#178;</code>
```&infin; ou &#8734;```    représente le symbole de l'infini       <code>&#8734;</code>


#### 2.3.Classement de la liste des entités W3C :

[**Lettres avec accents ou cédilles**](http://coin.des.experts.pagesperso-orange.fr/reponses/faq5_4a.html)
[**Caractères grecs**](http://coin.des.experts.pagesperso-orange.fr/reponses/faq5_4a.html)
[**Caractères monétaires ou juridiques**](http://coin.des.experts.pagesperso-orange.fr/reponses/faq5_4a.html)
[**Modificateurs de caractères**](http://coin.des.experts.pagesperso-orange.fr/reponses/faq5_4a.html)
[**Symboles de calcul élémentaire**](http://coin.des.experts.pagesperso-orange.fr/reponses/faq5_4a.html)
[**Symboles de mathématiques spécialisés**](http://coin.des.experts.pagesperso-orange.fr/reponses/faq5_4a.html)
[**Flèches**](http://coin.des.experts.pagesperso-orange.fr/reponses/faq5_4a.html)
[**Formes à caractère graphique**](http://coin.des.experts.pagesperso-orange.fr/reponses/faq5_4a.html)
[**Ligatures**](http://coin.des.experts.pagesperso-orange.fr/reponses/faq5_4a.html)
[**Ponctuation**](http://coin.des.experts.pagesperso-orange.fr/reponses/faq5_4a.html)
[**Autres**](http://coin.des.experts.pagesperso-orange.fr/reponses/faq5_4a.html)


#### 3.Ressources :

[par Alexandre Alapetite (source 2012-02-14)](https://alexandre.alapetite.fr/doc-alex/alx_special.html) -- Caractères spéciaux et entités HTML.
[site alexandre]: https://alexandre.alapetite.fr/doc-alex/alx_special.html
[par Karl Dubost/La Grange (source)](http://www.la-grange.net/w3c/html4.01/sgml/entities.html) -- Les références des entités de caractères dans le HTML4.
[par le Coin des experts (source)](http://coin.des.experts.pagesperso-orange.fr/reponses/faq5_4a.html) -- Classement de la liste des entités HTML proposées par le consortium W3C.
[par wikipedia (source)](https://fr.wikipedia.org/wiki/Liste_des_entit%C3%A9s_caract%C3%A8re_de_XML_et_HTML) -- Dans les langages de balisage comme SGML, HTML, XHTML et XML, une référence de caractère ou référence d'entité caractère, est une série de caractères qui représente un autre caractère.